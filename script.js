var urlErp = "https://erp.arco.sa:65//api/GetMyTicket?CustomerId=CIN0000150";


$(document).ready(function () {

    getTableList();

});

function getTableList() {
    debugger

    $("#loadingModal").show();

    $.get(urlErp, function(data,status){

        $("#loadingModal").hide();

        var ticketList = data.Data;
        $("#ticket-body").html("");
        for (i = 0; i < ticketList.length; i++) {

            var html = "<tr>" +

                "<td>" + ticketList[i].EnquiryId + "</td>" +
                "<td>" + ticketList[i].CreatedDatetime + "</td>" +
                "<td>" + ticketList[i].CreatedBy + "</td>" +
                "<td>" + ticketList[i].ContractNumber + "</td>" +
                "<td>" + ticketList[i].CustomerId + "</td>" +
                "<td>" + ticketList[i].CustomerName + "</td>" +
                "<td>" + ticketList[i].StatusName + "</td>" +
                "<td>" + ticketList[i].TicketTypeName + "</td>" +
                "<td>" + ticketList[i].PriorityName + "</td>" +
                "<td>" + ticketList[i].Subject + "</td>" +

                "</tr>";

            $("#ticket-body").append(html);
        }
        
    });
}


function getContractlist() {
    debugger

    $("#loadingModal").show();

    var urlContractNo = "https://erp.arco.sa:65/api/GetTicketCustomerDetails?CustomerId=CIN0000150";

    $.get(urlContractNo, function (data, status) {

        $("#loadingModal").hide();

        var contractNO = data.Result2;

        $("#contractnumber").html("");

        var emptyOption = "<Option></Option>";
        $("#contractnumber").append(emptyOption);

        for (i = 0; i < contractNO.length; i++) {
            var con = '<option value = "' + contractNO[i].ContractNumber + '">' +
                contractNO[i].ContractNumber + '</option>';

            $("#contractnumber").append(con);
        }
    });

}

$("#contractnumber").change(function () {
    getLabourid();

});

function getLabourid() {
    debugger

    $("#loadingModal").show();

    var urlLabourid = "https://erp.arco.sa:65//api/GetTicketIndContractAllEmployee?CustomerId=CIN0000150&ContractId=";

    var contractNO = $("#contractnumber").val();
    var urlLabourlist = urlLabourid + contractNO;

    $.get(urlLabourlist, function (data, status) {

        $("#loadingModal").hide();

        var labourId = data;

        $("#labourid").html("");

        var emptyOptionLabour = "<Option></Option>";
        $("#labourid").append(emptyOptionLabour);

        for (i = 0; i < labourId.length; i++) {
            var lab = '<option value ="' + labourId[i].EmployeeId + '">' +
                labourId[i].EmployeeId + '</option>';

            $("#labourid").append(lab);
        }
    });

}

$("#labourid").change(function () {
    getTicketType();

});

function getTicketType() {
    debugger

    $("#loadingModal").show();

    var urlticketType = "https://erp.arco.sa:65//api/TickettypeList";

    $.get(urlticketType, function (data, status) {

        $("#loadingModal").hide();

        var ticketType = data.Data;

        $("#tickettype").html("");

        var emptyOptionticketType = "<Option></Option>";
        $("#ticketType").append(emptyOptionticketType);

        for (i = 0; i < ticketType.length; i++) {
            var tick = '<option id="' + ticketType[i].ID + '" value= "' + ticketType[i].ID + '">' +
                ticketType[i].Name + '</option>';

            $("#tickettype").append(tick);
        }
    });

}

$("#tickettype").change(function () {
    getDepartmentType();

});

function getDepartmentType() {
    debugger

    $("#loadingModal").show();

    var urlDepartment = "https://erp.arco.sa:65//api/GetTicketAssignedToGroupByTicketTypeId?TicketTypeID=";
    var ticketType = $("#tickettype").val();
    var urldepartlist = urlDepartment + ticketType;

    $.get(urldepartlist, function (data, status) {

        $("#loadingModal").hide();

        var Depart = data;

        $("#department").html("");

        var emptyOptiondepart = "<option></option>"
        $("#department").append(emptyOptiondepart);

        for (i = 0; i < Depart.length; i++) {
            var dep = '<option value = "' + Depart[i].ID + '">' +
                Depart[i].Name + '</option>';

            $("#department").append(dep);
        }

    });
}

$("#department").change(function () {
    getAssignedto();

});

function getAssignedto() {
    debugger

    $("#loadingModal").show();

    var urlAssignto = "https://erp.arco.sa:65//api/assigntoList";

    $.get(urlAssignto, function (data, status) {

        $("#loadingModal").hide();

        var assignto = data.Data;

        $("#assignedto").html("");

        var emptyOptionAssign = "<Option></Option>"
        $("#assignedto").append(emptyOptionAssign);

        for (i = 0; i < assignto.length; i++) {
            var ass = '<option value = "' + assignto[i].ID + '">' +
                assignto[i].Name + '</option>';

            $("#assignedto").append(ass);
        }
    });
}

$("#assignedto").change(function () {
    getPriority();

});

function getPriority() {
    debugger

    $("#loadingModal").show();

    var urlPriority = "https://erp.arco.sa:65//api/PriorityList";

    $.get(urlPriority, function (data, status) {

        $("#loadingModal").hide();

        var Priority = data.Data;

        $("#priority").html("");

        var emptyOptionpriority = "<Option></Option>";
        $("#priority").append(emptyOptionpriority);

        for (i = 0; i < Priority.length; i++) {
            var pri = '<option value= "' + Priority[i].ID + '">' +
                Priority[i].Name + '</option>';

            $("#priority").append(pri);
        }
    });

}

$("#priority").change(function () {
    getCategorylist();

});

function getCategorylist() {
    debugger

        $("#loadingModal").show();

    var urlCategory = "https://erp.arco.sa:65//api/GetTicketGroupByDepatmentId?TicketAssignGroupId=";
    var depart = $("#department").val();
    var urlCategorylist = urlCategory + depart;

    $.get(urlCategorylist, function (data, status) {

        $("#loadingModal").hide();

        var Category = data;

        $("#category").html("");

        var emptyOptionCategory = "<Option></Option>";
        $("#category").append(emptyOptionCategory);

        for (i = 0; i < Category.length; i++) {
            var cat = '<option value= "' + Category[i].TicketGroupID + '">' +
                Category[i].TicketGroupName + '</option>';

            $("#category").append(cat);
        }
    });

}

$("#category").change(function () {
    getSubCategorylist();

});

function getSubCategorylist() {
    debugger

    $("#loadingModal").show();

    var urlSubCategory = "https://erp.arco.sa:65/api/SubGroupByGroup?id=";


    var Category = $("#category").val();

    var urlSubCategorylist = urlSubCategory + Category;

    $.get(urlSubCategorylist, function (data, status) {

        $("#loadingModal").hide();

        var subCategory = data;

        $("#subcategory").html("");

        var emptyOptionSubCategory = "<Option></Option>";
        $("#subcategory").append(emptyOptionSubCategory);

        for (i = 0; i < subCategory.length; i++) {
            var sub = '<option value = "' + subCategory[i].Id + '">' +
                subCategory[i].Description + '</option>';

            $("#subcategory").append(sub);
        }
    });
}

function Validationdata() {
    var urlApi = "https://erp.arco.sa:65//api/CreateTicketNew?UpdateTicketFields=";

    var tickparm = {

        cusnam: "",
        custid: "CIN0000150",
        emil: "",
        descp: $("#description").text,
        contno: "",
        Page: "",
        PageSize: "",
        Priority: $("#priority").val(),
        Group: $("#category").val(),
        SubGroup: $("#subcategory").val(),
        Subject: $("#ticketsubject").text,
        AssignedTo: $("#assignedto").val(),
        TicketType: $("#tickettype").val(),
        TicketAssignGroup: $("#department").val(),
        ContractNumber: $("#contractnumber").val(),
        LabourNumber: $("#labourid").val(),
        TicketChannel: 2,
        UserId: "cc.user",
        accuracy: "",

    }

    var tickreg = JSON.stringify(tickparm);

    var urlFields = urlApi + tickreg;

    $.post(urlFields, function (data, status) {

        alert(data);

        getTableList();
        $("#closemodal").click();
    });

}
